﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace Erasmus {
    public partial class Dashboard : UserControl {

        SqlConnection sqlConn;
        public static int id = 0;
        
        private static Dashboard _instance;

        public static Dashboard Instance {
            get {
                if (_instance == null)
                    _instance = new Dashboard();
                return _instance;
            }
        }

        public Dashboard() {
            InitializeComponent();
            // fill comboBox
            Program.fillComboBoxDashboard(sqlConn, comboBoxSearchDashboard);
            
            // get total users
            Program.totalUsers(sqlConn, lblUsersInSystem);
            
            // get all institutions
            Program.getAllInstitutions(sqlConn, comboBoxInstitutions);
        }

        // get institutions data
        private void comboBoxInstitutions_SelectedIndexChanged(object sender, EventArgs e) {
            String query = "SELECT name, course, country, age FROM students WHERE institution = '" +comboBoxInstitutions.Text+ "' ";
            Program.populateDataGridView(sqlConn, dataGridViewDashboard, query);
        }

        // get data from institutions
        private void comboBoxSearchDashboard_SelectedIndexChanged(object sender, EventArgs e) {
            // if no projects selected
            if (comboBoxSearchDashboard.Text == "Choose a project") {
                MessageBox.Show("Please select a project!", Program.CaptionMessageBox); 
            } else {
                String query = "SELECT " 
                               + " s.name, " 
                               + " course, " 
                               + " country, " 
                               + " average, " 
                               + " age, " 
                               + " erasmus_repeat, " 
                               + " institution FROM students s " 
                               + " JOIN project p ON s.project_id = p.id " 
                               + " JOIN users u ON u.id = p.id_user " 
                               + " WHERE p.name = '" +comboBoxSearchDashboard.Text+ "' ";
                
                // populate DataGridViewW
                Program.populateDataGridView(sqlConn, dataGridViewDashboard, query);
            }
        }
    }
}
