﻿namespace Erasmus
{
    partial class login
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblogin = new System.Windows.Forms.Label();
            this.lbpassword = new System.Windows.Forms.Label();
            this.tbusername = new System.Windows.Forms.TextBox();
            this.tbpassword = new System.Windows.Forms.TextBox();
            this.btlogin = new System.Windows.Forms.Button();
            this.btnew = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize) (this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Erasmus.Properties.Resources.erasmus_img_removebg_preview;
            this.pictureBox1.Location = new System.Drawing.Point(157, 27);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(882, 353);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 12;
            this.pictureBox1.TabStop = false;
            // 
            // lblogin
            // 
            this.lblogin.AutoSize = true;
            this.lblogin.Font = new System.Drawing.Font("Palatino Linotype", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.lblogin.ForeColor = System.Drawing.Color.White;
            this.lblogin.Location = new System.Drawing.Point(342, 481);
            this.lblogin.Name = "lblogin";
            this.lblogin.Size = new System.Drawing.Size(129, 32);
            this.lblogin.TabIndex = 14;
            this.lblogin.Text = "Username:";
            // 
            // lbpassword
            // 
            this.lbpassword.AutoSize = true;
            this.lbpassword.Font = new System.Drawing.Font("Palatino Linotype", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.lbpassword.ForeColor = System.Drawing.Color.White;
            this.lbpassword.Location = new System.Drawing.Point(349, 528);
            this.lbpassword.Name = "lbpassword";
            this.lbpassword.Size = new System.Drawing.Size(122, 32);
            this.lbpassword.TabIndex = 15;
            this.lbpassword.Text = "Password:";
            // 
            // tbusername
            // 
            this.tbusername.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.tbusername.Location = new System.Drawing.Point(477, 484);
            this.tbusername.Name = "tbusername";
            this.tbusername.Size = new System.Drawing.Size(362, 29);
            this.tbusername.TabIndex = 16;
            // 
            // tbpassword
            // 
            this.tbpassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.tbpassword.Location = new System.Drawing.Point(477, 531);
            this.tbpassword.Name = "tbpassword";
            this.tbpassword.Size = new System.Drawing.Size(362, 29);
            this.tbpassword.TabIndex = 17;
            this.tbpassword.UseSystemPasswordChar = true;
            // 
            // btlogin
            // 
            this.btlogin.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int) (((byte) (31)))), ((int) (((byte) (44)))), ((int) (((byte) (55)))));
            this.btlogin.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int) (((byte) (31)))), ((int) (((byte) (44)))), ((int) (((byte) (55)))));
            this.btlogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btlogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.btlogin.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btlogin.Location = new System.Drawing.Point(349, 595);
            this.btlogin.Name = "btlogin";
            this.btlogin.Size = new System.Drawing.Size(217, 30);
            this.btlogin.TabIndex = 18;
            this.btlogin.Text = "Login";
            this.btlogin.UseVisualStyleBackColor = true;
            this.btlogin.Click += new System.EventHandler(this.btlogin_Click);
            // 
            // btnew
            // 
            this.btnew.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int) (((byte) (31)))), ((int) (((byte) (44)))), ((int) (((byte) (55)))));
            this.btnew.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int) (((byte) (31)))), ((int) (((byte) (44)))), ((int) (((byte) (55)))));
            this.btnew.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnew.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.btnew.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnew.Location = new System.Drawing.Point(622, 595);
            this.btnew.Name = "btnew";
            this.btnew.Size = new System.Drawing.Size(217, 30);
            this.btnew.TabIndex = 19;
            this.btnew.Text = "New user";
            this.btnew.UseVisualStyleBackColor = true;
            this.btnew.Click += new System.EventHandler(this.btnew_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Palatino Linotype", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(434, 383);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(332, 86);
            this.label1.TabIndex = 23;
            this.label1.Text = "User login";
            // 
            // login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int) (((byte) (65)))), ((int) (((byte) (84)))), ((int) (((byte) (103)))));
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnew);
            this.Controls.Add(this.btlogin);
            this.Controls.Add(this.tbpassword);
            this.Controls.Add(this.tbusername);
            this.Controls.Add(this.lbpassword);
            this.Controls.Add(this.lblogin);
            this.Controls.Add(this.pictureBox1);
            this.Location = new System.Drawing.Point(15, 15);
            this.Name = "login";
            this.Size = new System.Drawing.Size(1200, 740);
            ((System.ComponentModel.ISupportInitialize) (this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        private System.Windows.Forms.Label label1;

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblogin;
        private System.Windows.Forms.Label lbpassword;
        private System.Windows.Forms.Button btlogin;
        private System.Windows.Forms.Button btnew;
        public System.Windows.Forms.TextBox tbusername;
        public System.Windows.Forms.TextBox tbpassword;
    }
}
