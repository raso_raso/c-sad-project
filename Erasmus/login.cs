﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;


namespace Erasmus {
    public partial class login : UserControl {
        
        private static login _instance;

        public static login Instance {
            get {
                if (_instance == null)
                    _instance = new login();
                return _instance;
            }
        }

        public login() {
            InitializeComponent();
        }

        private void btlogin_Click(object sender, EventArgs e) {
            SqlConnection sqlConn = new SqlConnection(Program.ConnectionString);
            
            try {
                SqlCommand cmd = new SqlCommand("select username, password from [users] where [username] = @username and [password] = @password", sqlConn);
                Console.WriteLine(Program.ConnectionString);

                // binding parameters
                cmd.Parameters.Add("@username", SqlDbType.VarChar).Value = tbusername.Text;
                cmd.Parameters.Add("@password", SqlDbType.VarChar).Value = Program.GetMD5(tbpassword.Text);

                // open connection
                sqlConn.Open();

                // get a SqlReader
                SqlDataReader reader = cmd.ExecuteReader();

                // check existing record
                if (reader.HasRows) {
                    while (reader.Read()) {

                        // check credentials
                        Program.Username = reader["username"].ToString();
                        Program.Password = reader["password"].ToString();
                        
                        Erasmus erasmus = (Erasmus)Application.OpenForms["Erasmus"];
                        Panel panel = (Panel)erasmus.Controls["panel"];
                        panel.AutoScroll = true;
                         
                        // to give scrolling option
                        // panel.AutoScrollMargin = new System.Drawing.Size(20, 490);
                        // panel.AutoScrollMinSize = new System.Drawing.Size(20, 490);
                        
                        panel.Controls.Add(Dashboard.Instance);
                        panel.Left = 160;
                        Dashboard.Instance.Dock = DockStyle.Fill;
                        Dashboard.Instance.BringToFront();

                        tbpassword.Text = string.Empty;
                        tbusername.Text = string.Empty;
                    }
                    // get userID
                    Program.getUserID();
                } else {
                    MessageBox.Show("Wrong username or password.", Program.CaptionMessageBox);
                    tbpassword.Text = string.Empty;
                    tbusername.Text = string.Empty;
                }
                reader.Close();
                sqlConn.Close();
            } catch (SqlException) { 
                MessageBox.Show("ERROR in database connection", Program.CaptionMessageBox);
                tbpassword.Text = string.Empty;
                tbusername.Text = string.Empty;
            }
        }

        // go to new account
        private void btnew_Click(object sender, EventArgs e) {
            Erasmus erasmus = (Erasmus)Application.OpenForms["Erasmus"];
            Panel panel = (Panel)erasmus.Controls["panel"];
            panel.Controls.Add(newaccount.Instance);
            newaccount.Instance.Dock = DockStyle.Fill;
            newaccount.Instance.BringToFront();
            tbpassword.Text = string.Empty;
            tbusername.Text = string.Empty;
        }

    }
}
