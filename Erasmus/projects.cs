﻿using System;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Erasmus {
    public partial class Projects : UserControl {
        
        SqlConnection sqlConn;

        private bool isClicked = false;
        private static Projects _instance;

        public static Projects Instance {
            get {
                if (_instance == null)
                    _instance = new Projects();
                return _instance;
            }
        }
        
        public Projects() {
            InitializeComponent();
            // fill comboBox
            Program.fillComboBoxSearchProject(sqlConn, comboBoxSearchProject);
            
            Console.WriteLine("Program.UserID ==>> " +Program.UserID);
            Dashboard.id = Program.UserID;
        }

        // search projects
        private void btsearchProject_Click(object sender, EventArgs e) {
            // if no projects selected
            if (comboBoxSearchProject.Text == "Choose a project") {
                MessageBox.Show("Please select a project!", Program.CaptionMessageBox); 
            } else {
                String searchProject = comboBoxSearchProject.Text;
                String query = "SELECT " 
                               + " s.name, " 
                               + " course, " 
                               + " country, " 
                               + " average, " 
                               + " age, " 
                               + " erasmus_repeat, " 
                               + " institution FROM students s " 
                               + " JOIN project p ON s.project_id = p.id " 
                               + " JOIN users u ON u.id = p.id_user " 
                               + " WHERE p.name = '" +searchProject+ "' ";
                
                // populate DataGridViewW
                lblTableNameSearch.Text = searchProject.ToUpper();
                Program.populateDataGridView(sqlConn, dataGridViewProject, query);
            }
        }

        // export results
        private void btnExport_Click(object sender, EventArgs e) {
            if (!Program.isExported) {
                MessageBox.Show("Please create a project before export", Program.CaptionMessageBox);
            } else {
                string input = Microsoft.VisualBasic.Interaction.InputBox("Export result Data", 
                    Program.CaptionMessageBox, 
                        "Filename...", 
                        -1, -1);
                Program.csvExport(input);
                MessageBox.Show("Results exported to " +Program.pathString, Program.CaptionMessageBox);
            }
        }
        
        // create view results for dataGridView
        public void viewResultsInDataGridView() {
            isClicked = true;
            // set a stringBuilder for the query results
            StringBuilder query = new StringBuilder();
            query.Append("SELECT DISTINCT " +
                         "name, " +
                         "course, " +
                         "country, " +
                         "average, " +
                         "age, " +
                         "erasmus_repeat, " +
                         "institution " +
                         "FROM " +AHPHelper.tempTable+ " WHERE");

            // TODO -> need to correct error when only one student is selected
            foreach (var s in Program.studentNames) {
                query.Append(" name = '" + s + "'");
                query.Append(" OR ");
            }

            // remove last 'OR '
            if (Program.studentNames.Count > 1) {
                query.Length -= 3;
            }

            String sqlQuery = query.ToString();
            Console.WriteLine(query);
            lblTableNameSearch.Text = "Project results".ToUpper();
            Program.populateDataGridView(sqlConn, dataGridViewProject, sqlQuery);
        }


    }
}