﻿namespace Erasmus {
    partial class Dashboard {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.lblTableName = new System.Windows.Forms.Label();
            this.lblDashboard = new System.Windows.Forms.Label();
            this.lblDashboard2 = new System.Windows.Forms.Label();
            this.lblInstitutions = new System.Windows.Forms.Label();
            this.lblUsersInSystem = new System.Windows.Forms.Label();
            this.comboBoxSearchDashboard = new System.Windows.Forms.ComboBox();
            this.dataGridViewDashboard = new System.Windows.Forms.DataGridView();
            this.comboBoxInstitutions = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize) (this.dataGridViewDashboard)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTableName
            // 
            this.lblTableName.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.lblTableName.Location = new System.Drawing.Point(19, 16);
            this.lblTableName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTableName.Name = "lblTableName";
            this.lblTableName.Size = new System.Drawing.Size(194, 37);
            this.lblTableName.TabIndex = 20;
            this.lblTableName.Text = "Dashboard";
            // 
            // lblDashboard
            // 
            this.lblDashboard.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.lblDashboard.Location = new System.Drawing.Point(19, 77);
            this.lblDashboard.Name = "lblDashboard";
            this.lblDashboard.Size = new System.Drawing.Size(229, 32);
            this.lblDashboard.TabIndex = 21;
            this.lblDashboard.Text = "Total existing projects:";
            // 
            // lblDashboard2
            // 
            this.lblDashboard2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.lblDashboard2.Location = new System.Drawing.Point(765, 77);
            this.lblDashboard2.Name = "lblDashboard2";
            this.lblDashboard2.Size = new System.Drawing.Size(203, 32);
            this.lblDashboard2.TabIndex = 22;
            this.lblDashboard2.Text = "Registered users:";
            // 
            // lblInstitutions
            // 
            this.lblInstitutions.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.lblInstitutions.Location = new System.Drawing.Point(423, 77);
            this.lblInstitutions.Name = "lblInstitutions";
            this.lblInstitutions.Size = new System.Drawing.Size(127, 35);
            this.lblInstitutions.TabIndex = 23;
            this.lblInstitutions.Text = "Institutions:";
            // 
            // lblUsersInSystem
            // 
            this.lblUsersInSystem.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.lblUsersInSystem.Location = new System.Drawing.Point(965, 77);
            this.lblUsersInSystem.Name = "lblUsersInSystem";
            this.lblUsersInSystem.Size = new System.Drawing.Size(50, 26);
            this.lblUsersInSystem.TabIndex = 24;
            this.lblUsersInSystem.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // comboBoxSearchDashboard
            // 
            this.comboBoxSearchDashboard.FormattingEnabled = true;
            this.comboBoxSearchDashboard.Location = new System.Drawing.Point(252, 83);
            this.comboBoxSearchDashboard.Margin = new System.Windows.Forms.Padding(2);
            this.comboBoxSearchDashboard.Name = "comboBoxSearchDashboard";
            this.comboBoxSearchDashboard.Size = new System.Drawing.Size(165, 21);
            this.comboBoxSearchDashboard.TabIndex = 26;
            this.comboBoxSearchDashboard.Text = "Select a project";
            this.comboBoxSearchDashboard.SelectedIndexChanged += new System.EventHandler(this.comboBoxSearchDashboard_SelectedIndexChanged);
            // 
            // dataGridViewDashboard
            // 
            this.dataGridViewDashboard.AllowUserToAddRows = false;
            this.dataGridViewDashboard.AllowUserToDeleteRows = false;
            this.dataGridViewDashboard.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewDashboard.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewDashboard.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewDashboard.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewDashboard.Location = new System.Drawing.Point(19, 190);
            this.dataGridViewDashboard.Margin = new System.Windows.Forms.Padding(2);
            this.dataGridViewDashboard.Name = "dataGridViewDashboard";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewDashboard.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridViewDashboard.RowHeadersWidth = 4;
            this.dataGridViewDashboard.RowTemplate.Height = 24;
            this.dataGridViewDashboard.Size = new System.Drawing.Size(1004, 433);
            this.dataGridViewDashboard.TabIndex = 27;
            // 
            // comboBoxInstitutions
            // 
            this.comboBoxInstitutions.FormattingEnabled = true;
            this.comboBoxInstitutions.Location = new System.Drawing.Point(555, 83);
            this.comboBoxInstitutions.Margin = new System.Windows.Forms.Padding(2);
            this.comboBoxInstitutions.Name = "comboBoxInstitutions";
            this.comboBoxInstitutions.Size = new System.Drawing.Size(165, 21);
            this.comboBoxInstitutions.TabIndex = 29;
            this.comboBoxInstitutions.Text = "Choose an institution";
            this.comboBoxInstitutions.SelectedIndexChanged += new System.EventHandler(this.comboBoxInstitutions_SelectedIndexChanged);
            // 
            // Dashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLight;
            this.Controls.Add(this.comboBoxInstitutions);
            this.Controls.Add(this.dataGridViewDashboard);
            this.Controls.Add(this.comboBoxSearchDashboard);
            this.Controls.Add(this.lblUsersInSystem);
            this.Controls.Add(this.lblInstitutions);
            this.Controls.Add(this.lblDashboard2);
            this.Controls.Add(this.lblDashboard);
            this.Controls.Add(this.lblTableName);
            this.Location = new System.Drawing.Point(15, 15);
            this.Name = "Dashboard";
            this.Size = new System.Drawing.Size(1200, 940);
            ((System.ComponentModel.ISupportInitialize) (this.dataGridViewDashboard)).EndInit();
            this.ResumeLayout(false);
        }

        #endregion
        
        public System.Windows.Forms.ComboBox comboBoxInstitutions;
        private System.Windows.Forms.DataGridView dataGridViewDashboard;
        public System.Windows.Forms.ComboBox comboBoxSearchDashboard;
        private System.Windows.Forms.Label lblDashboard2;
        private System.Windows.Forms.Label lblDashboard;
        private System.Windows.Forms.Label lblInstitutions;
        private System.Windows.Forms.Label lblUsersInSystem;
        private System.Windows.Forms.Label lblTableName;
    }
}
