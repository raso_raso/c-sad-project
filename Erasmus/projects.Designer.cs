﻿using System.ComponentModel;

namespace Erasmus {
    partial class Projects {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dataGridViewProject = new System.Windows.Forms.DataGridView();
            this.comboBoxSearchProject = new System.Windows.Forms.ComboBox();
            this.btsearchProject = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lblTableNameSearch = new System.Windows.Forms.Label();
            this.btnExport = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize) (this.dataGridViewProject)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewProject
            // 
            this.dataGridViewProject.AllowUserToAddRows = false;
            this.dataGridViewProject.AllowUserToDeleteRows = false;
            this.dataGridViewProject.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewProject.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewProject.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewProject.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewProject.Location = new System.Drawing.Point(19, 190);
            this.dataGridViewProject.Margin = new System.Windows.Forms.Padding(2);
            this.dataGridViewProject.Name = "dataGridViewProject";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewProject.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridViewProject.RowHeadersWidth = 4;
            this.dataGridViewProject.RowTemplate.Height = 24;
            this.dataGridViewProject.Size = new System.Drawing.Size(1004, 433);
            this.dataGridViewProject.TabIndex = 20;
            // 
            // comboBoxSearchProject
            // 
            this.comboBoxSearchProject.FormattingEnabled = true;
            this.comboBoxSearchProject.Location = new System.Drawing.Point(18, 630);
            this.comboBoxSearchProject.Margin = new System.Windows.Forms.Padding(2);
            this.comboBoxSearchProject.Name = "comboBoxSearchProject";
            this.comboBoxSearchProject.Size = new System.Drawing.Size(165, 21);
            this.comboBoxSearchProject.TabIndex = 19;
            this.comboBoxSearchProject.Text = "Choose a project";
            // 
            // btsearchProject
            // 
            this.btsearchProject.BackColor = System.Drawing.Color.Teal;
            this.btsearchProject.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btsearchProject.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int) (((byte) (31)))), ((int) (((byte) (44)))), ((int) (((byte) (55)))));
            this.btsearchProject.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int) (((byte) (31)))), ((int) (((byte) (44)))), ((int) (((byte) (55)))));
            this.btsearchProject.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btsearchProject.ForeColor = System.Drawing.SystemColors.Window;
            this.btsearchProject.Location = new System.Drawing.Point(19, 656);
            this.btsearchProject.Name = "btsearchProject";
            this.btsearchProject.Size = new System.Drawing.Size(164, 23);
            this.btsearchProject.TabIndex = 18;
            this.btsearchProject.Text = "Search";
            this.btsearchProject.UseVisualStyleBackColor = false;
            this.btsearchProject.Click += new System.EventHandler(this.btsearchProject_Click);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.label1.Location = new System.Drawing.Point(19, 11);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(371, 41);
            this.label1.TabIndex = 21;
            this.label1.Text = "e-Rasmus Projects";
            // 
            // lblTableNameSearch
            // 
            this.lblTableNameSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.lblTableNameSearch.Location = new System.Drawing.Point(313, 148);
            this.lblTableNameSearch.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTableNameSearch.Name = "lblTableNameSearch";
            this.lblTableNameSearch.Size = new System.Drawing.Size(400, 26);
            this.lblTableNameSearch.TabIndex = 22;
            this.lblTableNameSearch.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnExport
            // 
            this.btnExport.BackColor = System.Drawing.Color.Teal;
            this.btnExport.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnExport.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int) (((byte) (31)))), ((int) (((byte) (44)))), ((int) (((byte) (55)))));
            this.btnExport.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int) (((byte) (31)))), ((int) (((byte) (44)))), ((int) (((byte) (55)))));
            this.btnExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExport.ForeColor = System.Drawing.SystemColors.Window;
            this.btnExport.Location = new System.Drawing.Point(859, 656);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(164, 23);
            this.btnExport.TabIndex = 23;
            this.btnExport.Text = "Export results";
            this.btnExport.UseVisualStyleBackColor = false;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // Projects
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLight;
            this.Controls.Add(this.btnExport);
            this.Controls.Add(this.lblTableNameSearch);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dataGridViewProject);
            this.Controls.Add(this.comboBoxSearchProject);
            this.Controls.Add(this.btsearchProject);
            this.Name = "Projects";
            this.Size = new System.Drawing.Size(1200, 740);
            ((System.ComponentModel.ISupportInitialize) (this.dataGridViewProject)).EndInit();
            this.ResumeLayout(false);
        }

        #endregion
        
        private System.Windows.Forms.Button btnExport;
        public System.Windows.Forms.Label lblTableNameSearch;
        public System.Windows.Forms.ComboBox comboBoxSearchProject;
        private System.Windows.Forms.Button btsearchProject;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dataGridViewProject;
        
    }
}