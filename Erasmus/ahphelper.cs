﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace Erasmus {
    public partial class AHPHelper : UserControl {
        
        SqlConnection sqlConn;
        SqlDataReader reader;
        public static bool isImported = false;
        
        public static String tempTable = "temp_table";
        
        private static AHPHelper _instance;

        private String queryAhp;

        public static AHPHelper Instance {
            get {
                if (_instance == null)
                    _instance = new AHPHelper();
                return _instance;
            }
        }
        
        public AHPHelper() {
            InitializeComponent();
        }
        
        // trackBarsDefaultValues
        public void trackBarsDefaultValues() {
            tb1.Value = 8;
            tb2.Value = 8;
            tb3.Value = 8;
        }
        
        // import CSV
        private void btnImport_Click(object sender, EventArgs e) {
            // import file
            string xmlPath;
            var fd = new OpenFileDialog();
            if (fd.ShowDialog() == DialogResult.OK) {
                xmlPath = fd.FileName;
                isImported = true;
            } else {
                return;
            }

            // get DB connection
            sqlConn = new SqlConnection(Program.ConnectionString);
            try {
                using (sqlConn) {
                    // create table dynamically
                    // create students table from imported CSV file
                    // using (var sr = new StreamReader(xmlPath)) {
                    //     sqlConn.Open();
                    //     for (int i = 1; i < 1; i++)
                    //         sr.ReadLine();
                    //     // set a ',' delimiter for CSV
                    //     var data = sr.ReadLine().Split(',');
                    //
                    //     // remove path name from file and retrieve only filename
                    //     // string fileNameWithoutExt = Path.GetFileNameWithoutExtension(xmlPath);
                    //     // tableName = "esc_" +fileNameWithoutExt;
                    //     // tableName = "students_data";
                    //
                    // drop table if exists
                    // String cmdText = @"drop table if exists " + tableName + " ";
                    // SqlCommand cmd = new SqlCommand();
                    // cmd.Connection = sqlConn;
                    // cmd.CommandText = cmdText;
                    // cmd.CommandType = CommandType.Text;
                    // cmd.ExecuteNonQuery();
                    //
                    //     // set a stringbuilder to create table dynamically according with length of attributes
                    //     StringBuilder strinBuilderQuery = new StringBuilder();
                    //     strinBuilderQuery.Append("CREATE TABLE " +tableName+ " ");
                    //     strinBuilderQuery.Append(" ( ");
                    //
                    //     for (int i = 0; i < data.Length; i++) {
                    //         strinBuilderQuery.Append(data[i] + " VARCHAR(MAX)");
                    //         strinBuilderQuery.Append(", ");
                    //     }
                    //
                    //     // remove last ", "
                    //     if (data.Length > 1) {
                    //         strinBuilderQuery.Length -= 2;
                    //     }
                    //
                    //     strinBuilderQuery.Append(")");
                    //     SqlCommand sqlQuery = new SqlCommand(strinBuilderQuery.ToString(), sqlConn);
                    //     sqlQuery.ExecuteNonQuery();
                    // }
                    // sqlConn.Close();
                    
                    // create temp table for imported data if not exists
                    if (Program.TableExists(tempTable) == 0) {
                        sqlConn.Open();
                        StringBuilder strinBuilderQuery = new StringBuilder();
                        strinBuilderQuery.Append("CREATE TABLE " +tempTable+ " ");
                        strinBuilderQuery.Append("(");
                        strinBuilderQuery.Append("id int IDENTITY(1, 1) NOT NULL,");
                        strinBuilderQuery.Append("name varchar(MAX) COLLATE Latin1_General_CI_AS NULL,");
                        strinBuilderQuery.Append("course varchar(MAX) COLLATE Latin1_General_CI_AS NULL,");
                        strinBuilderQuery.Append("country varchar(MAX) COLLATE Latin1_General_CI_AS NULL,");
                        strinBuilderQuery.Append("average varchar(MAX) COLLATE Latin1_General_CI_AS NULL,");
                        strinBuilderQuery.Append("age varchar(MAX) COLLATE Latin1_General_CI_AS NULL,");
                        strinBuilderQuery.Append("erasmus_repeat varchar(MAX) COLLATE Latin1_General_CI_AS NULL,");
                        strinBuilderQuery.Append("institution varchar(MAX) COLLATE Latin1_General_CI_AS NULL,");
                        strinBuilderQuery.Append("english_level varchar(MAX) COLLATE Latin1_General_CI_AS NULL,");
                        strinBuilderQuery.Append("erasmus_experience varchar(100) COLLATE Latin1_General_CI_AS NULL,");
                        strinBuilderQuery.Append("average_calc varchar(100) COLLATE Latin1_General_CI_AS NULL"); 
                        strinBuilderQuery.Append(")");
                        Console.WriteLine(strinBuilderQuery.ToString());
                        SqlCommand sqlQuery = new SqlCommand(strinBuilderQuery.ToString(), sqlConn);
                        sqlQuery.ExecuteNonQuery();
                    }
                    sqlConn.Close();

                    // insert students data from imported CSV file
                    using (var sr = new StreamReader(xmlPath)) {
                        sqlConn.Open();
                        // skip first line
                        sr.ReadLine();
                        // start reading second line
                        while (sr.Peek() != -1) {
                            var line = sr.ReadLine();
                            // set a ',' delimiter for CSV
                            var data = line.Split(',');
                    
                            // set a stringBuilder to populate table dynamically according with length of attributes
                            StringBuilder strinBuilderQuery2 = new StringBuilder();
                            strinBuilderQuery2.Append("INSERT INTO " +tempTable+ " VALUES (");
                    
                            // loop data rows
                            for (int i = 0; i < data.Length; i++) {
                                strinBuilderQuery2.Append("'" + data[i] + "'");
                                strinBuilderQuery2.Append(", ");
                            }
                    
                            // remove last ', '
                            if (data.Length > 1) {
                                strinBuilderQuery2.Length -= 2;
                            }
                            strinBuilderQuery2.Append(")");
                    
                            SqlCommand sqlQuery2 = new SqlCommand(strinBuilderQuery2.ToString(), sqlConn);
                            sqlQuery2.ExecuteNonQuery();
                        }
                    }
                    // close connection
                    sqlConn.Close();
                    MessageBox.Show("Data uploaded into database...");
                }
            }
            catch (Exception ex) {
                // catch split exception on CSV file.
                MessageBox.Show("[ERROR] - Please review your file data.", Program.CaptionMessageBox);
                Console.WriteLine(" \nError - " + ex.Message);
            }

            // querying students from temp table attributes using AHP method
            String query = "SELECT " 
                           + " name, " 
                           + " course, " 
                           + " country, " 
                           + " average, " 
                           + " age, " 
                           + " erasmus_repeat, " 
                           + " institution " 
                           + " from " +tempTable+ " where id is not null " +queryAhp;

            // populate dataGrid and create checkBoxes with imported actual data
            Program.populateDataGridView(sqlConn, dataGridView1, query);
            Program.createCheckBox(dataGridView1);
        }
        
        // using trackBar for the AHP search method
        private void btnTrackbarSearch_Click(object sender, EventArgs e) {
            if (!isImported) {
                MessageBox.Show("Please import some data...", Program.CaptionMessageBox);
            } else {
                AHPMethod();
                searchAHP();
                Program.studentAttributes.Clear();
                Program.studentNames.Clear();
            }
        }

        // AHP method
        public void AHPMethod() {
            
            // creating double variables to get trackBar values
            double impF3F2;
            double impF2F3;
            double impF3F1;
            double impF1F3;
            double impF2F1;
            double impF1F2;
            double generalCIsum;
            double[,] general = new double[3, 5];

            // if trackBar value higher than 8
            if (tb1.Value > 8) {
                impF2F3 = Math.Abs(tb1.Value - 7);
                impF3F2 = Convert.ToDouble(1.0 / Math.Abs(tb1.Value - 7));
                // if trackBar value lower than 8
            } else {
                impF3F2 = Math.Abs(tb1.Value - 9);
                impF2F3 = Convert.ToDouble(1.0 / Math.Abs(tb1.Value - 9));
            }

            // if trackBar value higher than 8
            if (tb2.Value > 8) {
                impF1F2 = Math.Abs(tb2.Value -7);
                impF2F1 = Convert.ToDouble(1.0 / Math.Abs(tb2.Value - 7));
                // if trackBar value lower than 8
            } else {
                impF2F1 = Math.Abs(tb2.Value - 9);
                impF1F2 = Convert.ToDouble(1.0 / Math.Abs(tb2.Value - 9));
            }

            // if trackBar value higher than 8
            if (tb3.Value > 8) {
                impF1F3 = Math.Abs(tb3.Value - 7);
                impF3F1 = Convert.ToDouble(1.0 / Math.Abs(tb3.Value - 7));
                // if trackBar value lower than 8
            } else {
                impF3F1 = Math.Abs(tb3.Value - 9);
                impF1F3 = Convert.ToDouble(1.0 / Math.Abs(tb3.Value - 9));
            }

            // create a matrix with the values and calculate them
            general[0, 0] = 1.0;
            general[0, 1] = impF1F2;
            general[0, 2] = impF1F3;
            general[0, 3] = Math.Pow(general[0, 0] * general[0, 1] * general[0, 2], 1.0 / 3.0);
            general[1, 0] = impF2F1;
            general[1, 1] = 1.0;
            general[1, 2] = impF2F3;
            general[1, 3] = Math.Pow(general[1, 0] * general[1, 1] * general[1, 2],1.0/3.0);
            general[2, 0] = impF3F1;
            general[2, 1] = impF3F2;
            general[2, 2] = 1.0;
            general[2, 3] = Math.Pow(general[2, 0] * general[2, 1] * general[2, 2], 1.0 / 3.0);
            generalCIsum = Convert.ToDouble(general[0, 3] + general[1, 3] + general[2, 3]);
            general[0, 4] = general[0, 3] / generalCIsum;
            general[1, 4] = general[1, 3] / generalCIsum;
            general[2, 4] = general[2, 3] / generalCIsum;

            // get AHP values from database attributes
            String query = "SELECT id, english_level, average_calc, erasmus_experience FROM " +tempTable+ " ORDER BY id";

            sqlConn = new SqlConnection(Program.ConnectionString);
            SqlCommand cmd = new SqlCommand(query, sqlConn);
            
            // open connection
            sqlConn.Open();
            cmd.CommandType = CommandType.Text;
            reader = cmd.ExecuteReader();

            // row count
            int n = 0;
            if (reader.HasRows) {
                while (reader.Read()) {
                    n += 1;
                }
            }
            reader.Close();
            
            // create key/value pair with previous produced values
            double[,] evaluation = new double[n, 5];
            double[,] factor_1 = new double[n, n + 2];
            double[,] factor_2 = new double[n, n + 2];
            double[,] factor_3 = new double[n, n + 2];

            reader = cmd.ExecuteReader();
            
            // convert database values to doubles
            if (reader.HasRows) {
                int i = 0;
                while (reader.Read()) {
                    evaluation[i, 0] = Convert.ToDouble(reader["id"]);
                    evaluation[i, 1] = Convert.ToDouble(reader["english_level"]);
                    evaluation[i, 2] = Convert.ToDouble(reader["erasmus_experience"]);
                    evaluation[i, 3] = Convert.ToDouble(reader["average_calc"]);
                    i += 1;
                }
            }
            reader.Close();
            sqlConn.Close();

            // do a division of converted values by matrix values
            for (int i = 0; i < n ; i++) {
                for (int j = 0; j < n; j++) {
                    factor_1[i, j] = Convert.ToDouble(evaluation[i, 1] / evaluation[j, 1]);
                    factor_2[i, j] = Convert.ToDouble(evaluation[i, 2] / evaluation[j, 2]);
                    factor_3[i, j] = Convert.ToDouble(evaluation[i, 3] / evaluation[j, 3]);
                }
                factor_1[i, n] = 1;
                factor_2[i, n] = 1;
                factor_3[i, n] = 1;
            }

            double factor1CIsum = 0, factor2CIsum = 0, factor3CIsum = 0;
            
            // calculate Math.Pow
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++) {
                    factor_1[i, n] *= factor_1[i, j];
                    factor_2[i, n] *= factor_2[i, j];
                    factor_3[i, n] *= factor_3[i, j];
                }

                factor_1[i, n] = Math.Pow(factor_1[i, n], 1.0 / n);
                factor1CIsum += factor_1[i, n];
                
                factor_2[i, n] = Math.Pow(factor_2[i, n], 1.0 / n);
                factor2CIsum += factor_2[i, n];
                
                factor_3[i, n] = Math.Pow(factor_3[i, n], 1.0 / n);
                factor3CIsum += factor_3[i, n];
            }

            // divide Math.Pow by factor ones
            for (int i = 0; i < n; i++) {
                factor_1[i, n + 1] = factor_1[i, n] / factor1CIsum;
                factor_2[i, n + 1] = factor_2[i, n] / factor2CIsum;
                factor_3[i, n + 1] = factor_3[i, n] / factor3CIsum;
            }
            
            for (int i = 0; i < n; i++) {
                evaluation[i, 4] = factor_1[i, n + 1]* general[0, 4] + factor_2[i, n + 1]* general[1, 4] + factor_3[i, n + 1]* general[2, 4];
            }

            // add values to an array so a query can be made/used
            double[] scores = new double[n];
            for (int i = 0; i < n; i++) { 
                scores[i] = evaluation[i,4];
            }

            // sort the array values
            Array.Sort(scores);
            Array.Reverse(scores);

            // Console.WriteLine("EVALUATION");
            // for (int i = 0; i < evaluation.GetLength(0); i++) {
            //     for (int j = 0; j < evaluation.GetLength(1); j++) {
            //         Console.Write(evaluation[i,j]+  "\t");
            //     }
            //     Console.WriteLine();
            // }
            // Console.ReadLine();

            // query using CASE with the respective values from AHP method
            queryAhp = " ORDER BY CASE [id] ";
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++) {
                    // if matrix value equals latest value extend the query results
                    if (evaluation[j, 4] == scores[i]) {
                        queryAhp += " WHEN " + evaluation[j, 0] + " THEN " + (i+1);
                    }
                }
            }
            queryAhp += " End";
        }

        // get the AHP search result
        public void searchAHP() {
            String query = "select " 
                           + " name, " 
                           + " course, " 
                           + " country, " 
                           + " average, " 
                           + " age, " 
                           + " erasmus_repeat, " 
                           + " institution " 
                           + " from " +tempTable+ " where id is not null " +queryAhp;

            // label text
            lblProject.Text = "AHP best search";
            
            // populate dataGrid and create checkBoxes
            Program.populateDataGridView(sqlConn, dataGridView1, query);
            Program.createCheckBox(dataGridView1);
        }

        // table with results strong/weak points
        private void button1_Click(object sender, EventArgs e) {
            if (!isImported) {
                MessageBox.Show("Please select some students", Program.CaptionMessageBox);
            } else {
                // insert data into project table
                Program.projectName = Microsoft.VisualBasic.Interaction.InputBox("Insert name for the project",
                    Program.CaptionMessageBox,
                    "project name...",
                    -1, -1);
                
                using (SqlConnection con = new SqlConnection(Program.ConnectionString)) {
                    String insertIntoProject = "INSERT INTO project VALUES ( '" +Program.projectName+ "', " +Program.UserID+ ")";
                    Console.WriteLine(insertIntoProject);
                    using (SqlCommand cmd2 = new SqlCommand(insertIntoProject, con)) {
                        con.Open();
                        cmd2.ExecuteNonQuery();
                        con.Close();
                    }
                }

                // get last inserted project ID
                using (SqlConnection connection = new SqlConnection(Program.ConnectionString)) {
                    String query2 = "SELECT TOP 1 id FROM project ORDER BY id DESC";
                    using (SqlCommand command = new SqlCommand(query2, connection)) {
                        connection.Open();
                        using (SqlDataReader reader = command.ExecuteReader()) {
                            while (reader.Read()) {
                                Program.projectID = reader.GetInt32(0);
                                Console.WriteLine("Program.projectID " +Program.projectID);
                            }
                        }
                    }
                }
                
                // call Search panel
                Erasmus erasmus = (Erasmus)Application.OpenForms["Erasmus"];
                Panel panel = (Panel)erasmus.Controls["panel"];
                panel.AutoScroll = true;

                panel.Controls.Add(Projects.Instance);
                panel.Left = 160;
                Projects.Instance.Dock = DockStyle.Fill;
                Projects.Instance.BringToFront();

                // create project results
                Projects.Instance.viewResultsInDataGridView();
                
                // create project table with results
                Program.populateStudentsTable();
                
                // fill comboBox for search projects
                Program.fillComboBoxSearchProject(sqlConn, Projects.Instance.comboBoxSearchProject);

                // set flag to true
                Program.isExported = true;
            }
        }

        // get value from cell checkBox
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e) {
            if (dataGridView1.CurrentRow != null && dataGridView1.CurrentRow.Cells["Select"].Value == null ) {
                dataGridView1.CurrentRow.Cells["Select"].Value = true;
                
                // get values from row and set them into variables
                String name = dataGridView1.Rows[e.RowIndex].Cells["name"].Value.ToString();
                String course = dataGridView1.Rows[e.RowIndex].Cells["course"].Value.ToString();
                String country = dataGridView1.Rows[e.RowIndex].Cells["country"].Value.ToString();
                String average = dataGridView1.Rows[e.RowIndex].Cells["average"].Value.ToString();
                String age = dataGridView1.Rows[e.RowIndex].Cells["age"].Value.ToString();
                String erasmusRepeat = dataGridView1.Rows[e.RowIndex].Cells["erasmus_repeat"].Value.ToString();
                String institution = dataGridView1.Rows[e.RowIndex].Cells["institution"].Value.ToString();
                
                // add name to list
                Program.studentNames.Add(name);

                // add full attributes to list
                Program.studentAttributes.Add("('"
                                              + name + "', '"
                                              + course + "', '"
                                              + country + "', '"
                                              + average + "', '"
                                              + age + "', '"
                                              + erasmusRepeat + "', '"
                                              + institution + "', ");

                // remove last added value from lists
            } else if (dataGridView1.CurrentRow != null && dataGridView1.CurrentRow.Cells["Select"].Value != null && 
                       (bool)dataGridView1.CurrentRow.Cells["Select"].Value) {
                
                dataGridView1.CurrentRow.Cells["Select"].Value = false;
                dataGridView1.CurrentRow.Cells["Select"].Value = null;
                // remove last added value from list
                Program.studentNames.RemoveAt(Program.studentNames.Count - 1);
                Program.studentAttributes.RemoveAt(Program.studentAttributes.Count - 1);
            }
        }
        
        
    }
}