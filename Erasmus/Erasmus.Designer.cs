﻿using System.ComponentModel;

namespace Erasmus {
    partial class Erasmus {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            this.btlogout = new System.Windows.Forms.Button();
            this.btimport = new System.Windows.Forms.Button();
            this.btnProject = new System.Windows.Forms.Button();
            this.btnsearch = new System.Windows.Forms.Button();
            this.panel = new System.Windows.Forms.Panel();
            this.paneltop = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.bt_minimize = new System.Windows.Forms.Button();
            this.btclose = new System.Windows.Forms.Button();
            this.ctime = new System.Windows.Forms.Label();
            this.panelbottom = new System.Windows.Forms.Panel();
            this.lbsmallname = new System.Windows.Forms.Label();
            this.cdate = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.paneltop.SuspendLayout();
            this.panelbottom.SuspendLayout();
            this.SuspendLayout();
            // 
            // btlogout
            // 
            this.btlogout.FlatAppearance.BorderSize = 0;
            this.btlogout.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int) (((byte) (31)))), ((int) (((byte) (44)))), ((int) (((byte) (55)))));
            this.btlogout.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int) (((byte) (31)))), ((int) (((byte) (44)))), ((int) (((byte) (55)))));
            this.btlogout.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btlogout.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.btlogout.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btlogout.Location = new System.Drawing.Point(0, 160);
            this.btlogout.Name = "btlogout";
            this.btlogout.Size = new System.Drawing.Size(160, 40);
            this.btlogout.TabIndex = 0;
            this.btlogout.Text = "     Log Out";
            this.btlogout.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btlogout.UseVisualStyleBackColor = true;
            this.btlogout.Click += new System.EventHandler(this.btlogout_Click);
            // 
            // btimport
            // 
            this.btimport.FlatAppearance.BorderSize = 0;
            this.btimport.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int) (((byte) (31)))), ((int) (((byte) (44)))), ((int) (((byte) (55)))));
            this.btimport.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int) (((byte) (31)))), ((int) (((byte) (44)))), ((int) (((byte) (55)))));
            this.btimport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btimport.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.btimport.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btimport.Location = new System.Drawing.Point(0, 46);
            this.btimport.Name = "btimport";
            this.btimport.Size = new System.Drawing.Size(160, 40);
            this.btimport.TabIndex = 1;
            this.btimport.Text = "     Dashboard";
            this.btimport.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btimport.UseVisualStyleBackColor = true;
            this.btimport.Click += new System.EventHandler(this.btImport_Click);
            // 
            // btnProject
            // 
            this.btnProject.FlatAppearance.BorderSize = 0;
            this.btnProject.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int) (((byte) (31)))), ((int) (((byte) (44)))), ((int) (((byte) (55)))));
            this.btnProject.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int) (((byte) (31)))), ((int) (((byte) (44)))), ((int) (((byte) (55)))));
            this.btnProject.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProject.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.btnProject.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnProject.Location = new System.Drawing.Point(0, 88);
            this.btnProject.Name = "btnProject";
            this.btnProject.Size = new System.Drawing.Size(160, 40);
            this.btnProject.TabIndex = 2;
            this.btnProject.Text = "     AHP Helper";
            this.btnProject.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnProject.UseVisualStyleBackColor = true;
            this.btnProject.Click += new System.EventHandler(this.btnProject_Click_1);
            // 
            // btnsearch
            // 
            this.btnsearch.FlatAppearance.BorderSize = 0;
            this.btnsearch.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int) (((byte) (31)))), ((int) (((byte) (44)))), ((int) (((byte) (55)))));
            this.btnsearch.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int) (((byte) (31)))), ((int) (((byte) (44)))), ((int) (((byte) (55)))));
            this.btnsearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnsearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.btnsearch.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnsearch.Location = new System.Drawing.Point(0, 124);
            this.btnsearch.Name = "btnsearch";
            this.btnsearch.Size = new System.Drawing.Size(160, 40);
            this.btnsearch.TabIndex = 2;
            this.btnsearch.Text = "     Projects";
            this.btnsearch.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnsearch.UseVisualStyleBackColor = true;
            this.btnsearch.Click += new System.EventHandler(this.btnsearch_Click);
            // 
            // panel
            // 
            this.panel.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.panel.Location = new System.Drawing.Point(162, 40);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(1038, 700);
            this.panel.TabIndex = 6;
            // 
            // paneltop
            // 
            this.paneltop.BackColor = System.Drawing.Color.FromArgb(((int) (((byte) (31)))), ((int) (((byte) (44)))), ((int) (((byte) (55)))));
            this.paneltop.Controls.Add(this.label1);
            this.paneltop.Controls.Add(this.bt_minimize);
            this.paneltop.Controls.Add(this.btclose);
            this.paneltop.Dock = System.Windows.Forms.DockStyle.Top;
            this.paneltop.Location = new System.Drawing.Point(0, 0);
            this.paneltop.Name = "paneltop";
            this.paneltop.Size = new System.Drawing.Size(1200, 40);
            this.paneltop.TabIndex = 7;
            this.paneltop.MouseDown += new System.Windows.Forms.MouseEventHandler(this.paneltop_MouseDown);
            this.paneltop.MouseMove += new System.Windows.Forms.MouseEventHandler(this.paneltop_MouseMove);
            this.paneltop.MouseUp += new System.Windows.Forms.MouseEventHandler(this.paneltop_MouseUp);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Palatino Linotype", 13F);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(10, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 24);
            this.label1.TabIndex = 11;
            this.label1.Text = "e-Rasmus";
            // 
            // bt_minimize
            // 
            this.bt_minimize.FlatAppearance.BorderSize = 0;
            this.bt_minimize.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int) (((byte) (41)))), ((int) (((byte) (53)))), ((int) (((byte) (65)))));
            this.bt_minimize.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int) (((byte) (41)))), ((int) (((byte) (53)))), ((int) (((byte) (65)))));
            this.bt_minimize.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_minimize.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.bt_minimize.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.bt_minimize.Location = new System.Drawing.Point(1107, 0);
            this.bt_minimize.Name = "bt_minimize";
            this.bt_minimize.Size = new System.Drawing.Size(40, 40);
            this.bt_minimize.TabIndex = 10;
            this.bt_minimize.Text = "_";
            this.bt_minimize.UseVisualStyleBackColor = true;
            this.bt_minimize.Click += new System.EventHandler(this.bt_minimize_Click);
            // 
            // btclose
            // 
            this.btclose.FlatAppearance.BorderSize = 0;
            this.btclose.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int) (((byte) (41)))), ((int) (((byte) (53)))), ((int) (((byte) (65)))));
            this.btclose.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int) (((byte) (41)))), ((int) (((byte) (53)))), ((int) (((byte) (65)))));
            this.btclose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btclose.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.btclose.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btclose.Location = new System.Drawing.Point(1147, 0);
            this.btclose.Name = "btclose";
            this.btclose.Size = new System.Drawing.Size(40, 40);
            this.btclose.TabIndex = 9;
            this.btclose.Text = "X";
            this.btclose.UseVisualStyleBackColor = true;
            this.btclose.Click += new System.EventHandler(this.btclose_Click);
            // 
            // ctime
            // 
            this.ctime.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.ctime.ForeColor = System.Drawing.Color.White;
            this.ctime.Location = new System.Drawing.Point(0, 39);
            this.ctime.Name = "ctime";
            this.ctime.Size = new System.Drawing.Size(160, 25);
            this.ctime.TabIndex = 0;
            this.ctime.Text = "Hour";
            this.ctime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelbottom
            // 
            this.panelbottom.BackColor = System.Drawing.Color.FromArgb(((int) (((byte) (31)))), ((int) (((byte) (44)))), ((int) (((byte) (55)))));
            this.panelbottom.Controls.Add(this.lbsmallname);
            this.panelbottom.Controls.Add(this.cdate);
            this.panelbottom.Controls.Add(this.ctime);
            this.panelbottom.Location = new System.Drawing.Point(0, 640);
            this.panelbottom.Name = "panelbottom";
            this.panelbottom.Size = new System.Drawing.Size(160, 100);
            this.panelbottom.TabIndex = 8;
            // 
            // lbsmallname
            // 
            this.lbsmallname.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.lbsmallname.ForeColor = System.Drawing.Color.White;
            this.lbsmallname.Location = new System.Drawing.Point(0, 13);
            this.lbsmallname.Name = "lbsmallname";
            this.lbsmallname.Size = new System.Drawing.Size(160, 25);
            this.lbsmallname.TabIndex = 2;
            this.lbsmallname.Text = "Name";
            this.lbsmallname.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cdate
            // 
            this.cdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.cdate.ForeColor = System.Drawing.Color.White;
            this.cdate.Location = new System.Drawing.Point(0, 64);
            this.cdate.Name = "cdate";
            this.cdate.Size = new System.Drawing.Size(160, 25);
            this.cdate.TabIndex = 1;
            this.cdate.Text = "Date";
            this.cdate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.time_tick);
            // 
            // Erasmus
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int) (((byte) (65)))), ((int) (((byte) (84)))), ((int) (((byte) (103)))));
            this.ClientSize = new System.Drawing.Size(1200, 740);
            this.Controls.Add(this.panel);
            this.Controls.Add(this.panelbottom);
            this.Controls.Add(this.paneltop);
            this.Controls.Add(this.btimport);
            this.Controls.Add(this.btnProject);
            this.Controls.Add(this.btnsearch);
            this.Controls.Add(this.btlogout);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Erasmus";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Erasmus";
            this.paneltop.ResumeLayout(false);
            this.paneltop.PerformLayout();
            this.panelbottom.ResumeLayout(false);
            this.ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.Button btlogout;
        private System.Windows.Forms.Button btnsearch;
        private System.Windows.Forms.Button btimport;
        private System.Windows.Forms.Button btnProject;
        private System.Windows.Forms.Panel paneltop;
        private System.Windows.Forms.Panel panelbottom;
        private System.Windows.Forms.Button bt_minimize;
        private System.Windows.Forms.Button btclose;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label ctime;
        private System.Windows.Forms.Label cdate;
        public System.Windows.Forms.Panel panel;
        private System.Windows.Forms.Label lbsmallname;
    }
}