﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Deployment.Application;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace Erasmus {
    static class Program {
        
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main() {
            
            // to get console command prints in Rider JetBrains!!
            AttachConsole(-1);
            
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            
            // starter form
            Application.Run(new Erasmus()); 
        }
        
        // to get console command prints in Rider JetBrains!!
        [DllImport("kernel32.dll")]
        static extern bool AttachConsole(int dwProcessId);
        
        // attributes
        private static int userID;
        public static int projectID;
        private static string username;
        private static string password;
        public static string pathString;
        public static string tableNameProject;
        public static string projectName;
        
        public static List<String> studentNames = new List<string>();
        public static List<String> studentAttributes = new List<string>();
        
        public static bool isExported = false;

        // popUp caption message
        private static string captionMessageBox = "e-Rasmus";

        // connection string
        // private static string connectionString = "Data Source=localhost;Initial Catalog=erasmus;Persist Security Info=True;User ID=root;Password=rasosql";
        private static string connectionString = "Data Source=jduart.ddns.net,8080;Initial Catalog=erasmus_2;Persist Security Info=True;User ID=erasmus;Password=erasmus2021";

        // get sets of attributes
        public static int UserID { 
            get => userID; 
            set => userID = value;
        }

        public static string Username {
            get => username; 
            set => username = value;
        }

        public static string CaptionMessageBox {
            get => captionMessageBox;
        }

        public static string ConnectionString {
            get => connectionString;
        }
        public static String Password {
            get => password;
            set => password = value;
        }

        // encrypt password
        public static string GetMD5(string text) {

            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(text));
            byte[] result = md5.Hash;
            StringBuilder str = new StringBuilder();
            for (int i = 1; i < result.Length; i++) {
                str.Append(result[i].ToString("x2"));
            }
            return str.ToString();
        }
        
        // get user id
        public static void getUserID() {
            String query = "select id from users WHERE username = '" +username+ "' ";
            
            SqlConnection sqlConn = new SqlConnection(ConnectionString);
            SqlCommand cmd = new SqlCommand(query, sqlConn);
            
            // open connection
            sqlConn.Open();
            cmd.CommandType = CommandType.Text;
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read()) {
                UserID = Convert.ToInt32(dr["id"]); 
            }
        }

        // populate dataGridView
        public static void populateDataGridView(SqlConnection sqlConn, DataGridView dataGridView, String query) {
            // clear columns and rows
            dataGridView.Rows.Clear();
            dataGridView.Columns.Clear();
            
            sqlConn = new SqlConnection(ConnectionString);
            SqlCommand cmd = new SqlCommand(query, sqlConn);
            
            // open connection
            sqlConn.Open();
            cmd.CommandType = CommandType.Text;
            SqlDataReader dr = cmd.ExecuteReader();
            
            // fill headers
            int columnNumber = dr.FieldCount;
            for (int i = 0; i < columnNumber; i++) {
                dataGridView.Columns.Add(dr.GetName(i), dr.GetName(i));
                // align cells text to center
                dataGridView.Columns[i].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dataGridView.Columns[i].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            }
            
            // string array
            string[] dataLine = new string[columnNumber];

            // fill rows
            while (dr.Read()) {
                for (int a = 0; a < columnNumber; a++) {
                    if (dr.GetFieldType(a).ToString() == "System.Int32") {
                        dataLine[a] = dr.GetInt32(a).ToString();
                    }
                    if (dr.GetFieldType(a).ToString() == "System.String") {
                        dataLine[a] = dr.GetString(a);
                    }
                    if (dr.GetFieldType(a).ToString() == "System.DateTime") {
                        dataLine[a] = dr.GetDateTime(a).ToString();
                    }
                }
                // populate datagrid
                dataGridView.Rows.Add(dataLine);
            }
        }
        
        // fillComboBoxSearchProject from database
        public static void fillComboBoxSearchProject(SqlConnection sqlConn, ComboBox searchComboBox) {
            sqlConn = new SqlConnection(ConnectionString);
            SqlCommand cmd2 = new SqlCommand("select name from project p  where id_user = "+UserID+ " ", sqlConn);
            // open connection
            sqlConn.Open();
            cmd2.CommandType = CommandType.Text;
            SqlDataReader dr = cmd2.ExecuteReader();
            while (dr.Read()) {
                for (int i = 0; i < dr.FieldCount; i++) {
                    searchComboBox.Items.Add(dr.GetString(i));
                }
            }
        }
       
        // get all projects
        public static void fillComboBoxDashboard(SqlConnection sqlConn, ComboBox searchComboBox) {
            sqlConn = new SqlConnection(ConnectionString);
            SqlCommand cmd2 = new SqlCommand("SELECT name FROM project", sqlConn);
            // open connection
            sqlConn.Open();
            cmd2.CommandType = CommandType.Text;
            SqlDataReader dr = cmd2.ExecuteReader();
            while (dr.Read()) {
                for (int i = 0; i < dr.FieldCount; i++) {
                    searchComboBox.Items.Add(dr.GetString(i));
                }
            }
        }
        
        // count total users
        public static void totalUsers(SqlConnection sqlConn, Label lblUsersInSystem) {
            sqlConn = new SqlConnection(ConnectionString);
            SqlCommand cmd2 = new SqlCommand("SELECT COUNT(id) FROM users ", sqlConn);
            // open connection
            sqlConn.Open();
            cmd2.CommandType = CommandType.Text; 
            lblUsersInSystem.Text = Convert.ToInt32(cmd2.ExecuteScalar()).ToString();
        }
        
        // get all institutions
        public static void getAllInstitutions(SqlConnection sqlConn, ComboBox searchComboBox) {
            sqlConn = new SqlConnection(ConnectionString);
            SqlCommand cmd2 = new SqlCommand("SELECT DISTINCT institution FROM students", sqlConn);
            // open connection
            sqlConn.Open();
            cmd2.CommandType = CommandType.Text;
            SqlDataReader dr = cmd2.ExecuteReader();
            while (dr.Read()) {
                for (int i = 0; i < dr.FieldCount; i++) {
                    searchComboBox.Items.Add(dr.GetString(i));
                }
            }
        }

        // create checkBoxes
        public static void createCheckBox(DataGridView dataGridView) {
            // create checkBox
            DataGridViewCheckBoxColumn checkBoxColumn = new DataGridViewCheckBoxColumn();
            dataGridView.Columns.Add(checkBoxColumn);
            // set column header
            checkBoxColumn.Name = "Select";
            checkBoxColumn.HeaderText = "Select";
            // align center text
            checkBoxColumn.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            // disable user create new checkboxes
            dataGridView.AllowUserToAddRows = false;
        }

        // populate project table
        public static void populateStudentsTable() {
            using (SqlConnection con = new SqlConnection(connectionString)) {
                StringBuilder strinBuilderQuery = new StringBuilder();

                // loop data rows
                for (int i = 0; i < studentAttributes.Count; i++) {
                    strinBuilderQuery.Append("INSERT INTO students VALUES ");
                    strinBuilderQuery.Append(studentAttributes[i]+ "'" +projectID+ "')");
                }
                Console.WriteLine("query ==>> " +strinBuilderQuery.ToString());
                using (SqlCommand cmd = new SqlCommand(strinBuilderQuery.ToString(), con)) {
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }
        }
        
        // check if table exists
        public static int TableExists(string table) {
            Int32 exists = 0;
            string sql = "SELECT count(*) as IsExists FROM dbo.sysobjects where id = object_id('[dbo].[" +table+ "]')";
            using (SqlConnection conn = new SqlConnection(connectionString)) {
                SqlCommand cmd = new SqlCommand(sql, conn);
   
                try {
                    conn.Open();
                    exists = (Int32)cmd.ExecuteScalar();
                } catch (Exception ex) {        
                    MessageBox.Show(ex.Message);
                }
            }
            return exists;
        }

        // export results to CSV file
        public static void csvExport(String fileName) {
            SqlConnection sqlConn = new SqlConnection(connectionString);
            
            String query = "SELECT * FROM students WHERE project_id = " +projectID+ " ";
            String folderName = @"C:\e-Rasmus Results";

            // create directory
            pathString = Path.Combine(folderName);
            Directory.CreateDirectory(pathString);

            // add the file name to the path.
            pathString = Path.Combine(pathString, fileName+ ".csv");
            
            try {
                // check if file name exists
                if (File.Exists(pathString)) {
                    MessageBox.Show("File name already exists, please choose another name.", CaptionMessageBox);
                    String newFileName = Microsoft.VisualBasic.Interaction.InputBox("Export result Data",
                        Program.CaptionMessageBox,
                        "Filename...",
                        -1, -1);

                    pathString = Path.Combine(folderName);
                    Directory.CreateDirectory(pathString);

                    // add the file name to the path.
                    pathString = Path.Combine(pathString, newFileName+ ".csv");
                }

                sqlConn.Open();
                SqlCommand command = new SqlCommand(query, sqlConn);
                IDataReader reader = command.ExecuteReader();

                List<string> lines = new List<string>();
                String headerLine = "";
                
                    String[] columns = new string[reader.FieldCount];
                    for (int i = 0; i < reader.FieldCount; i++) {
                        columns[i] = reader.GetName(i);
                    }
                
                    headerLine = String.Join(", ", columns);
                    lines.Add(headerLine);

                // get data
                while (reader.Read()) {
                    object[] values = new object[reader.FieldCount];
                    reader.GetValues(values);
                    lines.Add(String.Join(", ", values));
                }

                // write data to file
                File.WriteAllLines(pathString, lines);
            }
            catch (Exception ex) {
                Console.WriteLine(ex.ToString());
            }
        }
        
        // restarts the app
        public static void restart() {
            try {
                Application.Restart();
            }
            catch (Exception ex) {
                var f = ApplicationDeployment.CurrentDeployment.DataDirectory +@"\RestartError.txt";
                var err = "Message :" + ex.Message + "<br/>" + Environment.NewLine + "StackTrace :" + ex.StackTrace +
                          "" + Environment.NewLine + "Date :" + DateTime.Now.ToString();
                if (!File.Exists(f)) {
                    File.WriteAllText(f, err);
                } else {
                    File.AppendAllText(f, err);
                }
            }
        }

    }
}