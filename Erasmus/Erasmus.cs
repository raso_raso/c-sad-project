﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Deployment.Application;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace Erasmus {
    public partial class Erasmus : Form {
        
        private static Erasmus _instance;

        // X/Y control
        int togmove;
        int mvalx;
        int mvaly;
        
        [DllImport("Gdi32.dll", EntryPoint = "CreateRoundRectRgn")]

        // panel props
        private static extern IntPtr CreateRoundRectRgn
        (
            int nLeftRect, 
            int nTopRect, 
            int nRightRect, 
            int nBottomRect, 
            int nWidthEllipse, 
            int nHeightEllipse 
        );
        
        public Erasmus() {
            InitializeComponent();
            // create panel
            FormBorderStyle = FormBorderStyle.None;
            Region = System.Drawing.Region.FromHrgn(CreateRoundRectRgn(0, 0, Width, Height, 20, 20));
            panel.Left = 0;
            panel.Controls.Add(login.Instance);
            login.Instance.Dock = DockStyle.Fill;
            login.Instance.BringToFront();
        }
        
        // control X/Y of form
        private void paneltop_MouseDown(object sender, MouseEventArgs e) {
            togmove = 1;
            mvalx = e.X;
            mvaly = e.Y;
        }

        // control X/Y off form
        private void paneltop_MouseUp(object sender, MouseEventArgs e) {
            togmove = 0;
        }

        // control X/Y off form
        private void paneltop_MouseMove(object sender, MouseEventArgs e) {
            if (togmove == 1) {
                SetDesktopLocation(MousePosition.X - mvalx, MousePosition.Y - mvaly);
            }
        }
        
        // hour tick
        public void time_tick(object sender, EventArgs e) {
            ctime.Text = DateTime.Now.ToString("HH:mm:ss");
            cdate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            lbsmallname.Text = Program.Username; 
        }

        // close btn
        private void btclose_Click(object sender, EventArgs e) {
            // exit application
            const string message = "Do you want to exit?";
            const string caption = "EXIT";
            var result = MessageBox.Show(message, caption + " - " + Program.CaptionMessageBox, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result == DialogResult.Yes) {
                Application.Exit();
            }
        }

        // minimize btn
        private void bt_minimize_Click(object sender, EventArgs e) {
            WindowState = FormWindowState.Minimized;
        }

        // project panel
        private void btnProject_Click_1(object sender, EventArgs e) {
            // clear lists
            Program.studentAttributes.Clear();
            Program.studentNames.Clear();
            
            // clear dataGridView
            AHPHelper.Instance.dataGridView1.Rows.Clear();
            AHPHelper.Instance.dataGridView1.Columns.Clear();
            
            // set flag to false again
            AHPHelper.isImported = false;
            
            // drop table if exists
            SqlConnection sqlConn = new SqlConnection(Program.ConnectionString);
            sqlConn.Open();
            String cmdText = @"drop table if exists " +AHPHelper.tempTable+ " ";
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = sqlConn;
            cmd.CommandText = cmdText;
            cmd.CommandType = CommandType.Text;
            cmd.ExecuteNonQuery();
            sqlConn.Close();
            
            if (!panel.Controls.Contains(AHPHelper.Instance)) {
                panel.Controls.Add(AHPHelper.Instance);
                AHPHelper.Instance.Dock = DockStyle.Fill;
                AHPHelper.Instance.BringToFront();
            } else {
                AHPHelper.Instance.BringToFront();
                panel.AutoScroll = true;
                AHPHelper.Instance.trackBarsDefaultValues();
            }
        }
        
        // search panel
        private void btImport_Click(object sender, EventArgs e) {
            if (!panel.Controls.Contains(Dashboard.Instance)) {
                panel.AutoScroll = false;
                panel.Controls.Add(Dashboard.Instance);
                Dashboard.Instance.Dock = DockStyle.Fill;
                Dashboard.Instance.BringToFront();
                panel.AutoScroll = false;
            } else {
                Dashboard.Instance.BringToFront();
                panel.AutoScroll = false;
            }
        }
        
        private void btnsearch_Click(object sender, EventArgs e) {
            if (!panel.Controls.Contains(Projects.Instance)) {
                panel.Controls.Add(Projects.Instance);
                Projects.Instance.Dock = DockStyle.Fill;
                Projects.Instance.BringToFront();
            } else {
                Projects.Instance.BringToFront();
                panel.AutoScroll = true;
            }
        }

        // log out
        private void btlogout_Click(object sender, EventArgs e) {
            // restarts the app
            Program.restart();
            Application.Run(_instance);
            // panel.Left = 0;
            // panel.AutoScroll = false;
            // panel.Controls.Add(login.Instance);
            // login.Instance.Dock = DockStyle.Fill;
            // login.Instance.BringToFront();
            // InitializeComponent();

        }
        
    }
}