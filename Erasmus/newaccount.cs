﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Erasmus {
    public partial class newaccount : UserControl {
        
        private static newaccount _instance;

       public static newaccount Instance {
            get {
                if (_instance == null)
                    _instance = new newaccount();
                return _instance;
            }
        }
       
        public newaccount() {
            InitializeComponent();
        }

        // create new account
        private void btnRegister_Click_1(object sender, EventArgs e) {
            // get connection
            SqlConnection sqlConn = new SqlConnection(Program.ConnectionString); 
            try { 
                sqlConn.Open();
                
                // make query
                SqlCommand query = new SqlCommand("SELECT * FROM [users] where username = @username;", sqlConn);
                query.Parameters.Add("@username", SqlDbType.VarChar).Value = userRegTxt.Text;
            
                SqlDataReader reader = query.ExecuteReader();
                // if user exists in DB
                if (reader.HasRows) {
                    MessageBox.Show("Username already taken.", Program.CaptionMessageBox);
                    return;
                }
                // close reader
                reader.Close();

                    // close connection
                    sqlConn.Close();

                    //else add new user
                    using (sqlConn) {
                        try {
                        
                        using (var cmd = new SqlCommand("INSERT INTO [users] (username, password) VALUES (@username, @password)", sqlConn)) {

                            String user = userRegTxt.Text;
                            String pass = passRegTxt.Text;

                            // binding parameters and hashing password
                            cmd.Parameters.AddWithValue("@username", user);
                            cmd.Parameters.AddWithValue("@password", SqlDbType.VarChar).Value = Program.GetMD5(pass);

                            // check for empty fields
                            if (user.Equals("") || pass.Equals("")) {
                                MessageBox.Show("No empty fields allowed!", Program.CaptionMessageBox);
                                
                            } else {
                                sqlConn.Open();
                                if (cmd.ExecuteNonQuery() > 0) {
                                    MessageBox.Show("User inserted", Program.CaptionMessageBox);
                                    
                                } else {
                                    MessageBox.Show("User failed", Program.CaptionMessageBox);
                                }
                                // close DB connection
                                sqlConn.Close();

                                // open login form
                                Erasmus login = (Erasmus)Application.OpenForms["ERasmus"];
                                Panel panel = (Panel)login.Controls["panel"];
                                panel.Controls.Add(global::Erasmus.login.Instance);
                                global::Erasmus.login.Instance.Dock = DockStyle.Fill;
                                global::Erasmus.login.Instance.BringToFront();

                                foreach (Control c in Controls) {
                                    if (c.GetType() == typeof(TextBox)) {
                                        ((TextBox)(c)).Text = string.Empty;
                                    }
                                    if (c.GetType() == typeof(MaskedTextBox)) {
                                        ((MaskedTextBox)(c)).Text = string.Empty;
                                    }
                                }
                            }
                        }
                    } catch (Exception exc) {
                        MessageBox.Show("Error during insert: " + exc.Message);
                    }
                }
            } catch (SqlException) {
                MessageBox.Show("ERROR in database connection" +Program.CaptionMessageBox);
            }
        }

        // back btn
        private void btnBack_Click(object sender, EventArgs e) {
            // get back to login form
            Erasmus eRasmusForm = (Erasmus)Application.OpenForms["Erasmus"];
            Panel panel = (Panel)eRasmusForm.Controls["panel"];
            panel.Left = 0;
            foreach (Control c in Controls) {
                if (c.GetType() == typeof(TextBox)) {
                    ((TextBox)(c)).Text = string.Empty;
                }
                if (c.GetType() == typeof(MaskedTextBox)) {
                    ((MaskedTextBox)(c)).Text = string.Empty;
                }
            }
            panel.Controls.Add(login.Instance);
            login.Instance.Dock = DockStyle.Fill;
            login.Instance.BringToFront();
        }
    }
}
